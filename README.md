# Customers API #

A Mule API for the Customers Database

This API allows consumers to retrieve, create, update and delete customer details over HTTP. The messages are in JSON format.

### Version
1.0.0

## Allowed methods/endpoints 
Below is a summary of the API endpoints. The verbose version is found on the RAML API specification within the repository,

### GET: api/customers
Returns a list of customers 

*  This will return an array of active customers and their addresses and can be filtered by an exact firstName or lastName. In the RAML Design stage, I have considered other parameters to filter the results, that will hopefully be implemented in the future. 

### GET: api/customers/{id}
Retuns a customer resource

*  This endpoint returns an active customer resource and his/her addresses.

### POST: api/customers
Allows a user to create a new customer resource

*  Creates a new customer record. The API only allows for one customer to be created at a time.

### PUT: api/customers/{id}
Allows the user to update a customer resource

* Update firstName, lastName and addresses of a customer already in the database.
* If the resource is not found, a 404 response will be returned.
* Address Id is optional. If an Id is not found for an address group, a new address will be created for that customer

### PUT: api/customers
Allows the user to batch update customer resources

*  The API allows for batch update of existing customers and their addresses
*  Customer Id is required and must be supplied on the JSON message
*  Address Id is optional. If an Id is not found for an address group, a new address will be created for that customer

### DELETE api/customers/{id}
Deletes a customer resource

*  No content/body is necessary for this request
*  This request only does a tag deletion and not an actual delete. It will end-date the resource instead.

### DELETE api/customers/{id}/address{addressId}
Deletes a customer address

*  This is the only endpoint that is nested on the address level. I thought that it would be more intuitive to expose this endpoint as consumers may at some point require to delete an address for a customer (as part of an update).

## Data Design
The back-end of this API is a SQL Server Database called MyCustomers with the following tables:

### Customer
A basic table containing the first and last name of a customer

*  Id
*  FirstName
*  LastName
*  Create/Modify columns: CreatedDate, UpdatedDate, LastUpdatedBy and LastUpdatedDate
*  Active indicators: StartDate, EndDate

### Address
A basic table containing addresses of a customer

*  Id
*  CustomerId (FK) - A customer can have one or more addresses
*  AddressLine
*  City
*  PostCode
*  Country
*  Create/Modify columns: CreatedDate, UpdatedDate, LastUpdatedBy and LastUpdatedDate
*  Active indicators: StartDate, EndDate 

## Design Considerations
I spent quite a bit of time designing this API mainly because of the requirement that a customer can have "addresses". This API is not strictly REST but I've decided on this design for ease of use and maintainability. I thought about a few real-life scenarios in the context of a customer (i.e. getting married/changing surnames, moving to another house/changing address). As you will notice, the bulk of the effort is on the update side to allow for easier synchronisation. It is not by all means a perfect API but it satisfies the minimum and can be scaled in the future.
